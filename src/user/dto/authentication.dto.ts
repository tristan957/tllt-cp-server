import { ApiModelProperty } from "@nestjs/swagger";
import { IsString } from "class-validator";

export default class AuthenticationDto {
	@ApiModelProperty({ description: "Email of the user" })
	@IsString()
	public readonly email: string;

	@ApiModelProperty({ description: "Password of the user" })
	@IsString()
	public readonly password: string;
}
