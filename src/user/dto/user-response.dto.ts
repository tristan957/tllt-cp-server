import { ApiModelProperty } from "@nestjs/swagger";
import User from "../user.entity";
import { RecipeResponseDto } from "../../recipe/dto";

export default class UserResponseDto {
	@ApiModelProperty({ description: "ID of the user" })
	public readonly id: number;

	@ApiModelProperty({ description: "Name of the user" })
	public readonly name: string;

	@ApiModelProperty({ description: "Email of the user" })
	public readonly email: string;

	@ApiModelProperty({ description: "Date the user created the account" })
	public readonly createdAt: Date;

	@ApiModelProperty({ description: "Recipes authored by the user" })
	public readonly recipes: RecipeResponseDto[];

	public static from(user: User): UserResponseDto {
		// eslint-disable-next-line @typescript-eslint/no-unused-vars
		const { updatedAt, password, recipes, ...userResponse } = user;

		return {
			...userResponse,
			recipes:
				recipes !== undefined ? recipes.map(recipe => RecipeResponseDto.from(recipe, user)) : []
		};
	}
}
