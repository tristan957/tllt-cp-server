import { ApiModelProperty } from "@nestjs/swagger";
import { Min, Max } from "class-validator";

export default class UserFeedbackDto {
	@ApiModelProperty({ description: "Feedback of the user" })
	@Min(1) // eslint-disable-line no-magic-numbers
	@Max(3) // eslint-disable-line no-magic-numbers
	public readonly feedback: number;
}
