import { ApiModelProperty } from "@nestjs/swagger";
import { IsString } from "class-validator";

export default class FindUserByQueryDto {
	@ApiModelProperty({ description: "Email to search for" })
	@IsString()
	public readonly email: string;
}
