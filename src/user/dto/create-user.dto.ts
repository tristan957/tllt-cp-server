import { ApiModelProperty } from "@nestjs/swagger";
import { IsString, IsEmail } from "class-validator";

export default class CreateUserDto {
	@ApiModelProperty({ description: "Name of the user" })
	@IsString()
	public readonly name: string;

	@ApiModelProperty({ description: "Email of the user" })
	@IsEmail()
	public readonly email: string;

	@ApiModelProperty({ description: "Password of the user" })
	@IsString()
	public readonly password: string;
}
