import UserService from "./user.service";
import {
	Body,
	Controller,
	Post,
	Get,
	Param,
	HttpException,
	HttpStatus,
	Query,
	HttpCode
} from "@nestjs/common";
import {
	CreateUserDto,
	UserResponseDto,
	FindUserByQueryDto,
	AuthenticationDto,
	UserFeedbackDto
} from "./dto";
import { ApiResponse } from "@nestjs/swagger";
import { CreateRecipeDto, RecipeResponseDto, CookingDetailsResponseDto } from "../recipe/dto";

@Controller("users")
export default class UserController {
	private readonly userService: UserService;

	public constructor(userService: UserService) {
		this.userService = userService;
	}

	@Post("authenticate")
	@HttpCode(HttpStatus.ACCEPTED)
	@ApiResponse({ status: HttpStatus.ACCEPTED, description: "User authenticated" })
	@ApiResponse({ status: HttpStatus.BAD_REQUEST, description: "User does not exist" })
	public async authenticate(
		@Body() authenticationDto: AuthenticationDto
	): Promise<UserResponseDto> {
		const user = await this.userService.authenticate(authenticationDto);
		if (user === undefined) {
			throw new HttpException(`No user exists`, HttpStatus.BAD_REQUEST);
		}

		return UserResponseDto.from(user);
	}

	@Get(":id") // eslint-disable-line no-magic-numbers
	@ApiResponse({ status: HttpStatus.OK, description: "User found" })
	@ApiResponse({ status: HttpStatus.BAD_REQUEST, description: "User does not exist" })
	public async findUserById(@Param("id") id: number): Promise<UserResponseDto> {
		const user = await this.userService.findUserById(id);
		if (user === undefined) {
			throw new HttpException(`No user exists with an ID of ${id}`, HttpStatus.BAD_REQUEST);
		}

		return UserResponseDto.from(user);
	}

	@Get()
	@ApiResponse({ status: HttpStatus.OK, description: "User found" })
	@ApiResponse({ status: HttpStatus.BAD_REQUEST, description: "User does not exist" })
	public async findUserByQuery(@Query() query: FindUserByQueryDto): Promise<UserResponseDto> {
		const user = await this.userService.findUserByQuery(query);
		if (user === undefined) {
			throw new HttpException("No user exists", HttpStatus.BAD_REQUEST);
		}

		return UserResponseDto.from(user);
	}

	@Post()
	@ApiResponse({ status: HttpStatus.CREATED, description: "User created" })
	public async createUser(@Body() createUserDto: CreateUserDto): Promise<UserResponseDto> {
		return UserResponseDto.from(await this.userService.createUser(createUserDto));
	}

	@Post(":id/recipes")
	public async addRecipe(
		@Param("id") id: number,
		@Body() createRecipeDto: CreateRecipeDto
	): Promise<RecipeResponseDto> {
		const recipe = await this.userService.addRecipe(id, createRecipeDto);
		if (recipe === undefined) {
			throw new HttpException("Unable to create recipe", HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return RecipeResponseDto.from(recipe);
	}

	@Get(":userId/recipes/:recipeId/cook")
	public async cookRecipe(
		@Param("userId") userId: number,
		@Param("recipeId") recipeId: number
	): Promise<CookingDetailsResponseDto> {
		const response = await this.userService.cookRecipe(userId, recipeId);
		if (response === undefined) {
			throw new HttpException(
				"Unable to retrieve cooking details",
				HttpStatus.INTERNAL_SERVER_ERROR
			);
		}

		return response;
	}

	@Post(":userId/userScale")
	public async updateUserScale(
		@Body() userFeedback: UserFeedbackDto,
		@Param("userId") userId: number
	): Promise<void | undefined> {
		return this.userService.updateScale(userFeedback, userId);
	}
}
