import {
	Column,
	CreateDateColumn,
	Entity,
	PrimaryGeneratedColumn,
	UpdateDateColumn,
	OneToMany
} from "typeorm";
import { Recipe } from "../recipe";
import { IsEmail, Min, Max } from "class-validator";

export enum UserFeedback {
	LESS_TOASTY = 1, // eslint-disable-line no-magic-numbers
	THE_SAME,
	MORE_TOASTY
}

@Entity()
export default class User {
	@PrimaryGeneratedColumn({ name: "user_id" })
	public id: number;

	@Column({ type: "text" })
	public name: string;

	@Column({ type: "text" })
	public password: string;

	@Column({
		type: "varchar",
		length: 128
	})
	@IsEmail()
	public email: string;

	@CreateDateColumn({ name: "created_at" })
	public createdAt: Date;

	@UpdateDateColumn({ name: "updated_at" })
	public updatedAt: Date;

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	@OneToMany(type => Recipe, recipe => recipe.author, { cascade: true })
	public recipes: Recipe[];

	@Column({ type: "double", default: 0.5 })
	@Min(0) // eslint-disable-line no-magic-numbers
	@Max(1) // eslint-disable-line no-magic-numbers
	public scale: number;

	@Column({ default: 1 })
	public iterations: number;
}
