import { Module } from "@nestjs/common";
import UserController from "./user.controller";
import UserService from "./user.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import User from "./user.entity";
import { Recipe, RecipeModule } from "../recipe";

@Module({
	imports: [TypeOrmModule.forFeature([Recipe, User]), RecipeModule],
	controllers: [UserController],
	providers: [UserService],
	exports: [UserService]
})
// eslint-disable-next-line @typescript-eslint/no-extraneous-class
export default class UserModule {}
