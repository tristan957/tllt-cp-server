import { Repository } from "typeorm";
import User, { UserFeedback } from "./user.entity";
import { Injectable, Inject } from "@nestjs/common";
import { CreateUserDto, FindUserByQueryDto, AuthenticationDto, UserFeedbackDto } from "./dto";
import { InjectRepository } from "@nestjs/typeorm";
import { CreateRecipeDto, CookingDetailsResponseDto } from "../recipe/dto";
import { Recipe, RecipeService } from "../recipe";
import axios from "axios";

@Injectable()
export default class UserService {
	private readonly userRepository: Repository<User>;
	private readonly recipeService: RecipeService;

	public constructor(
		@InjectRepository(User) userRepository: Repository<User>,
		@Inject(RecipeService) recipeService: RecipeService
	) {
		this.userRepository = userRepository;
		this.recipeService = recipeService;
	}

	public async authenticate(authenticationDto: AuthenticationDto): Promise<User | undefined> {
		const user = await this.userRepository.findOne({
			where: authenticationDto,
			relations: ["recipes"]
		});

		return user;
	}

	public async findUserById(id: number): Promise<User | undefined> {
		const user = await this.userRepository.findOne(id, { relations: ["recipes"] });

		return user;
	}

	public async findUserByQuery(query: FindUserByQueryDto): Promise<User | undefined> {
		const user = await this.userRepository.findOne({ where: query, relations: ["recipes"] });

		return user;
	}

	public async createUser(createUserDto: CreateUserDto): Promise<User> {
		const user = await this.userRepository.save(this.userRepository.create(createUserDto));

		return user;
	}

	public async updateScale(
		userFeedbackDto: UserFeedbackDto,
		userId: number
	): Promise<void | undefined> {
		if (userFeedbackDto.feedback === UserFeedback.THE_SAME) {
			return;
		}

		const user = await this.findUserById(userId);
		if (user === undefined) {
			return undefined;
		}

		// eslint-disable-next-line no-magic-numbers
		const val = userFeedbackDto.feedback === UserFeedback.LESS_TOASTY ? 0 : 1;
		// eslint-disable-next-line no-magic-numbers
		user.scale = (user.scale * user.iterations + val) / (user.iterations + 1);
		user.iterations++;

		await this.userRepository.save(user);
	}

	public async addRecipe(
		authorId: number,
		createRecipeDto: CreateRecipeDto
	): Promise<Recipe | undefined> {
		const user = await this.userRepository.findOne(authorId, { relations: ["recipes"] });
		if (user === undefined) {
			return undefined;
		}

		const recipe = await this.recipeService.createRecipe(createRecipeDto, user);

		return recipe;
	}

	public async cookRecipe(
		authorId: number,
		recipeId: number
	): Promise<CookingDetailsResponseDto | undefined> {
		const user = await this.userRepository.findOne(authorId);
		if (user === undefined) {
			return undefined;
		}

		const recipe = await this.recipeService.findById(recipeId);
		if (recipe === undefined) {
			return undefined;
		}

		let sendingFoodNumber = 0;
		switch (recipe.type) {
			case 1: // eslint-disable-line no-magic-numbers
				break;
			case 2: // eslint-disable-line no-magic-numbers
				sendingFoodNumber = 1; // eslint-disable-line no-magic-numbers
				break;
			case 3: // eslint-disable-line no-magic-numbers
				sendingFoodNumber = 0.6; // eslint-disable-line no-magic-numbers
				break;
			case 4: // eslint-disable-line no-magic-numbers
				sendingFoodNumber = 0.3; // eslint-disable-line no-magic-numbers
				break;
			default:
				break;
		}

		let recipeObject;
		if (process.env.TLLT_CP_SERVER_BYPASS_NN !== undefined) {
			switch (sendingFoodNumber) {
				case 0: // eslint-disable-line no-magic-numbers
				default:
					recipeObject = {
						temperature: 425,
						time: 120
					};
					break;
				case 0.3: // eslint-disable-line no-magic-numbers
					recipeObject = {
						temperature: 425,
						time: 120
					};
					break;
				case 0.6: // eslint-disable-line no-magic-numbers
					recipeObject = {
						temperature: 425,
						time: 180
					};
					break;
				case 1: // eslint-disable-line no-magic-numbers
					recipeObject = {
						temperature: 425,
						time: 600
					};
					break;
			}
			return recipeObject;
		}

		// make request to Alex NN to get details
		const host = process.env.TLLT_CP_NN_SERVER || "http://localhost:9000";
		recipeObject = await axios.get(`${host}/tllt`, {
			params: {
				userScale: 0.2,
				foodType: sendingFoodNumber
			}
		});

		return recipeObject.data;
	}
}
