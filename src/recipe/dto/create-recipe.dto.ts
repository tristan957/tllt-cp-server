import { ApiModelProperty } from "@nestjs/swagger";
import { IsString, MaxLength, IsInt, Min, Max } from "class-validator";
import { MAX_RECIPE_NAME_LENGTH } from "../../constants";
import { RecipeType } from "../recipe.entity";

export default class CreateRecipeDto {
	@ApiModelProperty({ description: "Name of the recipe" })
	@IsString()
	@MaxLength(MAX_RECIPE_NAME_LENGTH)
	public name: string;

	// eslint-disable-next-line no-magic-numbers
	@ApiModelProperty({ description: "Type of the recipe", enum: [1, 2, 3, 4] })
	@IsInt()
	@Min(1) // eslint-disable-line no-magic-numbers
	@Max(4) // eslint-disable-line no-magic-numbers
	public readonly type: RecipeType;
}
