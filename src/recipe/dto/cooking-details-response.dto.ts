export default class CookingDetailsResponseDto {
	public readonly time: number;
	public readonly temperature: number;
}
