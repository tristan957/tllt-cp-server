import { ApiModelProperty } from "@nestjs/swagger";
import Recipe, { RecipeType } from "../recipe.entity";
import { User } from "../../user";

export default class RecipeResponseDto {
	@ApiModelProperty({ description: "ID of the user" })
	public readonly id: number;

	@ApiModelProperty({ description: "Name of the user" })
	public readonly name: string;

	@ApiModelProperty({ description: "Type of the recipe" })
	public readonly type: RecipeType;

	@ApiModelProperty({ description: "Author of the recipe" })
	public authorId: number;

	public static from(recipe: Recipe, author?: User): RecipeResponseDto {
		if (author === undefined) {
			// eslint-disable-next-line @typescript-eslint/no-unused-vars, no-shadow
			const { updatedAt, author, ...recipeResponseDto } = recipe;

			return { ...recipeResponseDto, authorId: author.id };
		}

		// eslint-disable-next-line @typescript-eslint/no-unused-vars
		const { updatedAt, ...recipeResponseDto } = recipe;

		return { ...recipeResponseDto, authorId: author.id };
	}
}
