import {
	Entity,
	PrimaryGeneratedColumn,
	Column,
	ManyToOne,
	CreateDateColumn,
	UpdateDateColumn
} from "typeorm";
import { User } from "../user";
import { MAX_RECIPE_NAME_LENGTH } from "../constants";
import { IsInt, Min, Max } from "class-validator";

export enum RecipeType {
	TOAST = 1, // eslint-disable-line no-magic-numbers
	PIZZA,
	BAGEL,
	POPTART
}

@Entity()
export default class Recipe {
	@PrimaryGeneratedColumn({ name: "recipe_id" })
	public id: number;

	@Column({ type: "varchar", length: MAX_RECIPE_NAME_LENGTH })
	public name: string;

	@Column({ type: "enum", enum: RecipeType })
	@IsInt()
	@Min(1) // eslint-disable-line no-magic-numbers
	@Max(4) // eslint-disable-line no-magic-numbers
	public type: RecipeType;

	@CreateDateColumn({ name: "created_at" })
	public createdAt: Date;

	@UpdateDateColumn({ name: "updated_at" })
	public updatedAt: Date;

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	@ManyToOne(type => User, user => user.recipes)
	public author: User;
}
