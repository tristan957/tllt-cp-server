import { Injectable } from "@nestjs/common";
import { Repository } from "typeorm";
import Recipe from "./recipe.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { CreateRecipeDto } from "./dto";
import { User } from "../user";

@Injectable()
export default class RecipeService {
	private readonly recipeRepository: Repository<Recipe>;

	public constructor(@InjectRepository(Recipe) recipeRepository: Repository<Recipe>) {
		this.recipeRepository = recipeRepository;
	}

	public async findById(recipeId: number): Promise<Recipe | undefined> {
		return this.recipeRepository.findOne(recipeId);
	}

	public async createRecipe(createRecipe: CreateRecipeDto, author: User): Promise<Recipe> {
		let recipe = this.recipeRepository.create(createRecipe);
		recipe.author = author;
		recipe = await this.recipeRepository.save(recipe);

		return recipe;
	}
}
