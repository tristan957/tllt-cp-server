import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import Recipe from "./recipe.entity";
import RecipeService from "./recipe.service";

@Module({
	imports: [TypeOrmModule.forFeature([Recipe])],
	providers: [RecipeService],
	exports: [RecipeService]
})
// eslint-disable-next-line @typescript-eslint/no-extraneous-class
export default class RecipeModule {}
