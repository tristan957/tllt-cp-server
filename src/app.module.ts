import { Module } from "@nestjs/common";
import { UserModule } from "./user";
import { TypeOrmModule } from "@nestjs/typeorm";
import { RecipeModule } from "./recipe";

@Module({
	imports: [
		TypeOrmModule.forRoot({
			type: "mariadb",
			host: process.env.TLLT_CP_DATABASE_HOST!,
			port: parseInt(process.env.TLLT_CP_DATABASE_PORT!, 10),
			username: process.env.TLLT_CP_DATABASE_USERNAME!,
			password: process.env.TLLT_CP_DATABASE_PASSWORD!,
			database: process.env.TLLT_CP_DATABASE!,
			entities: [`${__dirname}/../**/*.entity{.ts,.js}`],
			synchronize: process.env.NODE_ENV !== "production"
		}),
		UserModule,
		RecipeModule
	]
})
// eslint-disable-next-line @typescript-eslint/no-extraneous-class
export default class ApplicationModule {}
