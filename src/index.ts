import dotenv from "dotenv";

// Load env file before anything else happens
dotenv.config();

import { ValidationPipe } from "@nestjs/common";
import { NestFactory } from "@nestjs/core";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";
import ApplicationModule from "./app.module";

async function bootstrap(): Promise<void> {
	const app = await NestFactory.create(ApplicationModule, { cors: true });
	app.setGlobalPrefix("/api/v1");
	app.useGlobalPipes(new ValidationPipe());

	const options = new DocumentBuilder()
		.setTitle("The Learning Little Toaster Control Panel Server")
		.setDescription("The Learning Little Toaster Control Panel API description")
		.setVersion("1.0.0")
		.setBasePath("api/v1")
		.build();
	const document = SwaggerModule.createDocument(app, options);
	SwaggerModule.setup("api/v1/swagger", app, document);

	// eslint-disable-next-line no-magic-numbers
	await app.listen(8080);
}

bootstrap();
